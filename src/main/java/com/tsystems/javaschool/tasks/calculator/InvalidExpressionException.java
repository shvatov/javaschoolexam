package com.tsystems.javaschool.tasks.calculator;

/**
 * Defines a basic exception, which occurs if the input expression
 * fails the validation in the {@link ExpressionConverter} class constructor.
 *
 * @author Sergey Khvatov
 */
public class InvalidExpressionException extends Exception {
    public InvalidExpressionException() {
        super();
    }
}
