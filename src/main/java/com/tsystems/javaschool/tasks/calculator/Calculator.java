package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.Stack;

/**
 * Calculates the value of the mathematical statement using stack.
 *
 * @author Sergey Khvatov
 */
public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        try {
            String[] tokenArray = new ExpressionConverter().convert(statement);
            Stack<Double> result = new Stack<>();
            for (String token : tokenArray) {
                if (token.length() == 1 && ExpressionConverter.isOperator(token.charAt(0))) {
                    if (result.size() < 2) {
                        throw new InvalidExpressionException();
                    }
                    double b = result.pop();
                    double a = result.pop();
                    result.push(operation(token, a, b));
                } else {
                    result.push(Double.parseDouble(token));
                }
            }
            return new DecimalFormat("#.####").format(result.peek()).replace(',', '.');
        } catch (InvalidExpressionException iex) {
            return null;
        }
    }

    /**
     * Performs an operation according to its string representation.
     *
     * @param operator String representation of the operator.
     * @param a        Left operand of the operator.
     * @param b        Right operand of the operator.
     * @return result of the operation.
     * @throws InvalidExpressionException if arithmetic exception occurs.
     */
    private static double operation(String operator, double a, double b) throws InvalidExpressionException {
        switch (operator) {
            case "+":
                return a + b;
            case "-":
                return a - b;
            case "*":
                return a * b;
            case "/":
                if (isZero(b)) {
                    throw new InvalidExpressionException();
                }
                return a / b;
        }
        return 0;
    }

    /**
     * Checks if input double number is approximately zero.
     *
     * @param value Value to check.
     * @return True, if abs(value) is lesser than Double.MIN_VALUE,
     * false otherwise.
     */
    private static boolean isZero(double value) {
        return Math.abs(value) <= Double.MIN_VALUE;
    }
}
