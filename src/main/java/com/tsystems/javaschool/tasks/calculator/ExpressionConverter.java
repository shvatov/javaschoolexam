package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Validates the input math expression and if it is correct,
 * then translates it into reverse polish notation (RPN).
 *
 * @author Sergey Khvatov
 */
public class ExpressionConverter {
    /**
     * Array of chars, where each char represents
     * a basic valid operator, that can be used in the expression.
     */
    private static char[] VALID_OPEARTORS = {'+', '-', '/', '*'};

    /**
     * Opening bracket symbol.
     */
    private static char OPEN_BRACKET = '(';

    /**
     * Closing bracket symbol.
     */
    private static char CLOSE_BRACKET = ')';

    /**
     * Special symbol that is used to split the expressions into tokens.
     */
    private static String SPLIT_SYMBOL = "@";

    /**
     * Symbol, which represents a decimal point delimiter.
     */
    private static char DECIMAL_MARK = '.';

    /**
     * Converts input math expression from infix form to postfix
     * using reverse polish notation.
     *
     * @param expression math expression that must be converted from infix to postfix form.
     * @return Array of tokens, that form input math expression in postfix order.
     */
    public String[] convert(String expression) throws InvalidExpressionException {
        if (!validate(expression)) {
            throw new InvalidExpressionException();
        }

        String[] tokens = splitExpression(expression);
        Stack<String> stack = new Stack<>();
        StringBuilder rpn = new StringBuilder();
        for (String token : tokens) {
            // Consider that there are no unary operators
            // in the alphabet of operators used in the
            // math statement

            // process open bracket
            if (token.equals("" + OPEN_BRACKET)) {
                stack.push(token);
            }
            // process close bracket
            else if (token.equals("" + CLOSE_BRACKET)) {
                if (stack.isEmpty()) {
                    throw new InvalidExpressionException();
                }
                while (!stack.empty() && !stack.peek().equals("" + OPEN_BRACKET)) {
                    rpn.append(stack.peek());
                    rpn.append(SPLIT_SYMBOL);
                    stack.pop();
                }
                if (stack.empty()) {
                    throw new InvalidExpressionException();
                }
                stack.pop();
            }
            // process binary operator
            else if (token.length() == 1 && isOperator(token.charAt(0))) {
                if (!stack.empty() && !stack.peek().equals("" + OPEN_BRACKET)) {
                    while (!stack.empty() && !stack.peek().equals("" + OPEN_BRACKET) && priority(token.charAt(0)) <= priority(stack.peek().charAt(0))) {
                        rpn.append(stack.peek());
                        rpn.append(SPLIT_SYMBOL);
                        stack.pop();
                    }
                }
                stack.push(token);
            }
            // try processing numbers
            else {
                try {
                    double operand = Double.parseDouble(token);
                    rpn.append(operand);
                    rpn.append(SPLIT_SYMBOL);
                } catch (NumberFormatException nex) {
                    throw new InvalidExpressionException();
                }
            }
        }
        while (!stack.empty()) {
            rpn.append(stack.pop());
            rpn.append(SPLIT_SYMBOL);
        }
        return splitExpression(rpn.toString());
    }

    /**
     * Checks if input symbol is a valid operator.
     *
     * @param symbol Input symbol.
     * @return True, if symbol is an operator, false otherwise.
     */
    public static boolean isOperator(char symbol) {
        for (char op : VALID_OPEARTORS) {
            if (op == symbol) {
                return true;
            }
        }
        return false;
    }

    /**
     * Splits input expression into tokens.
     *
     * @param expression Input math expression.
     * @return String array with all the tokens from the input expression.
     */
    private static String[] splitExpression(String expression) {
        StringBuilder builder = new StringBuilder();
        char[] tempExpression = expression.replaceAll("\\s+", "").toCharArray();
        for (char symbol : tempExpression) {
            if (isOperator(symbol) || isBracket(symbol)) {
                builder.append(SPLIT_SYMBOL);
                builder.append(symbol);
                builder.append(SPLIT_SYMBOL);
            } else {
                builder.append(symbol);
            }
        }
        return deleteEmptyStrings(builder.toString().split(SPLIT_SYMBOL));
    }

    /**
     * Deletes all the empty strings from the input array.
     *
     * @param arr Input array of strings.
     * @return The same array but without empty strings.
     */
    private static String[] deleteEmptyStrings(String[] arr) {
        ArrayList<String> res = new ArrayList<>();
        for (String s : arr) {
            if (!s.isEmpty()) {
                res.add(s);
            }
        }
        return res.toArray(new String[0]);
    }

    /**
     * Performs a basic validation of the input math expression.
     *
     * @param expression Input math expression.
     * @return True, if this expression is correct, false otherwise.
     */
    private static boolean validate(String expression) {
        // check if string is empty
        if (expression.isEmpty()) {
            return false;
        }

        // check the number of the brackets
        // and if expression contains inappropriate symbols
        int openBrackets = 0, closeBrackets = 0;
        for (char ch : expression.toCharArray()) {
            if (!isValidSymbol(ch)) {
                return false;
            }
            if (ch == OPEN_BRACKET) {
                openBrackets++;
            } else if (ch == CLOSE_BRACKET) {
                closeBrackets++;
            }
        }
        return openBrackets == closeBrackets;
    }

    /**
     * Checks whether input symbol is valid or not.
     *
     * @param symbol Input symbol from math statement.
     * @return True, if symbol is valid, false otherwise.
     */
    private static boolean isValidSymbol(char symbol) {
        return isOperator(symbol) || symbol == DECIMAL_MARK || isBracket(symbol) || Character.isDigit(symbol);
    }

    /**
     * Checks if input symbol is a bracket.
     *
     * @param symbol Input symbol.
     * @return True, if symbol is a bracket, false otherwise.
     */
    private static boolean isBracket(char symbol) {
        return symbol == OPEN_BRACKET || symbol == CLOSE_BRACKET;
    }

    /**
     * Return the priority of the operator.
     *
     * @param operator Operator symbol.
     * @return priority of the operator
     */
    private static int priority(char operator) {
        switch (operator) {
            case '+':
            case '-':
                return 1;
            case '/':
            case '*':
                return 2;
            default:
                return 0;
        }
    }
}
