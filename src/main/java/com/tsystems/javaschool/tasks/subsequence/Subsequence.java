package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // assume that if case of incorrect parameters
        // we should return false
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        // can't form a bigger set from a smaller one
        if (x.size() > y.size()) {
            return false;
        }

        // check if there are some null references in x
        for (Object elem: x) {
            if (elem == null) {
                return false;
            }
        }

        // check if there are some null references in y
        for (Object elem: y) {
            if (elem == null) {
                return false;
            }
        }

        // go through all the elements of the y set
        // and check whether y contains elements to
        // form x set without changing the order
        int currentIndex = 0;
        for (int i = 0; i < y.size() && currentIndex < x.size(); i++) {
            if (y.get(i).equals(x.get(currentIndex))) {
                currentIndex++;
            }
        }
        // index must be equal to the size of the x set
        // if all the elements of the x set
        // are in the y set in the same order
        return currentIndex == x.size();
    }
}
