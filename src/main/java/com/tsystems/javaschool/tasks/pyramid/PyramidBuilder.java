package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // check if reference is null
        if (inputNumbers == null) {
            throw new CannotBuildPyramidException();
        }

        // check the size of the list
        if (inputNumbers.isEmpty() || inputNumbers.size() >= Integer.MAX_VALUE - 8) {
            // we cannot build from empty list
            // or if it's size bigger than Integer.MAX_VALUE - 8
            // cause it may lead to OutOfMemory exception
            throw new CannotBuildPyramidException();
        }

        // check if elements are not null
        for (Integer i : inputNumbers) {
            if (i == null) {
                throw new CannotBuildPyramidException();
            }
        }

        // sort list using integer comparator
        inputNumbers.sort(Integer::compare);

        // get the number of rows
        int rowsNum = 0, processedElements = 0, elementsNum = 0;
        while (processedElements < inputNumbers.size()) {
            rowsNum++;
            elementsNum++;
            processedElements += elementsNum;
        }
        // calculate the number of columns
        int columnNum = 2 * elementsNum - 1;

        if (processedElements != inputNumbers.size()) {
            throw new CannotBuildPyramidException();
        }

        // result matrix
        int[][] result = new int[rowsNum][columnNum];
        int toProcess = 1,  // number of elements from the list to be processed during the iteration
            offset = rowsNum - 1, // start offset
            index = 0;  // index of the next element from the list
        // depending on the number of the columns the
        // indices of non-zero elements may be odd or even
        boolean isOdd = (columnNum / 2) % 2 != 0;

        for (int i = 0; i < rowsNum; i++) {
            int processed = 0;
            for (int j = 0; j < columnNum; j++) {
                // add offset zeros
                if (j < offset || j >= columnNum - offset || processed == toProcess) {
                    result[i][j] = 0;
                }
                // add numbers
                else {
                    // if number of the row is even
                    if (isOdd) {
                        if (j % 2 == 1) {
                            result[i][j] = inputNumbers.get(index++);
                            processed++;
                        } else {
                            result[i][j] = 0;
                        }
                    } else {
                        if (j % 2 == 0) {
                            result[i][j] = inputNumbers.get(index++);
                            processed++;
                        } else {
                            result[i][j] = 0;
                        }
                    }
                }
            }
            offset--;
            toProcess++;
            isOdd = !isOdd;
        }
        return result;
    }
}
